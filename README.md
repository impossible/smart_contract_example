Smart contract example:

contract.sol holds the smart contract itself

contract.json holds the smart contract's interface (ABI)

contract.address holds the address where an instance of the contract is deployed on Ethereum TestNet

index.html holds a tiny website that visualizes this smart contract's data