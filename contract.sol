contract SomeBook {
    // Ethereum account address
    address owner;

    // List of previous owners
    address[] previous_owners;

    // hash of book content/id (md5 for example)
    bytes32 book_id;

    // Constructor - runs once when contract is deployed
    function SomeBook(bytes32 id){
        book_id = id;
        owner = msg.sender;
    }

    // Read
    function getBookId() constant returns (bytes32) {
        return book_id;
    }

    function getOwner() constant returns (address) {
        return owner;
    }

    function getOwnerHistory() constant returns (address[]) {
        return previous_owners;
    }

    // Write
    function giveOwnership(address new_owner) {
        if (msg.sender != owner) {
            throw;
        }
        previous_owners.length++;
        previous_owners[previous_owners.length-1] = owner;
        owner = new_owner;
    }

}